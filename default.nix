{nixpkgs?import<nixpkgs>{}}:

with nixpkgs;

let
  nontexed = import ./nontexed/default.nix { nixpkgs = nixpkgs; };
  py3 = "${python3.withPackages(ps: [])}/bin/python3";
in

stdenv.mkDerivation {
  name = "studyos";
  src = ./src;
  buildInputs = [ makeWrapper xorg.libxcb ];
  installPhase = ''
    mkdir -p $out/bin $out/lib $out/lib/browser
    cp $src/*.py $src/*.html $src/*.txt $out/lib
    cc $src/browser/ld_preload.c -o $out/lib/browser/libld_preload.so -lxcb -ldl -shared
    makeWrapper ${py3} $out/bin/studyos --add-flags $out/lib/main.py --prefix PYTHONPATH : ${nontexed}/lib --prefix PATH : ${lib.makeBinPath [ awesome firefox xorg.xgamma ]}
  '';
}
