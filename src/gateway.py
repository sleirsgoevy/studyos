import http.server, os.path, threading, fileserver, musicserver, tbserver, nontexedweb, timetable

class GatewayRequestHandler(
    fileserver.VerySecureRequestHandler,
    musicserver.MusicRequestHandler,
    tbserver.TBServer,
    timetable.TimeTableServer,
    nontexedweb.NontexedRequestHandler):
    def dosmth(self, method):
        path = self.path
        print(method, path)
        if path.startswith('/files/'):
            self.path = self.path[6:] or '/'
            return getattr(fileserver.VerySecureRequestHandler, method)(self)
        elif path.startswith('/music/'):
            self.path = self.path[6:] or '/'
            return getattr(musicserver.MusicRequestHandler, method)(self)
        elif path.startswith('/tb/'):
            self.path = self.path[3:] or '/'
            return getattr(tbserver.TBServer, method)(self)
        elif path.startswith('/nontexed/'):
            self.path = self.path[9:] or '/'
            return getattr(nontexedweb.NontexedRequestHandler, method)(self)
        elif path.startswith('/timetable/'):
            self.path = self.path[10:] or '/'
            return getattr(timetable.TimeTableServer, method)(self)
    def do_GET(self):
        if self.path in ('/', '/music.html', '/ntwrap.html'):
            file = 'index.html' if self.path == '/' else self.path[1:]
            path = os.path.join(os.path.split(__file__)[0], file)
            with open(path, 'rb') as file:
                file.seek(0, os.SEEK_END)
                self.send_response(200)
                self.send_header('Content-Type', 'text/html')
                self.send_header('Content-Length', file.tell())
                self.end_headers()
                file.seek(0)
                while True:
                    chk = file.read(4096)
                    self.wfile.write(chk)
            return
        return self.dosmth('do_GET')
    def do_POST(self):
        return self.dosmth('do_POST')

def run_server():
    os.chdir(os.path.join(os.environ['HOME'], '.studyos', 'secureroot'))
    srv = nontexedweb.create_server(('127.0.0.1', 0), GatewayRequestHandler)
    threading.Thread(target=srv.serve_forever).start()
    return srv.server_address[1]
