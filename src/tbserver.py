import http.server, time, os, json, html, run_browser, threading

def battery():
    bats = 0
    batn = 0
    charging = False
    for i in os.listdir('/sys/class/power_supply'):
        with open('/sys/class/power_supply/'+i+'/uevent') as f:
            for l in f:
                if l.startswith('POWER_SUPPLY_CAPACITY='):
                    x = int(l[22:])
                    bats += x
                    batn += 1
                elif l.strip() == 'POWER_SUPPLY_STATUS=Charging':
                    charging = True
    return 50, True # (bats // batn if batn else 100), charging

def get_statusline():
    t = time.localtime()
    b, c = battery()
    return '%02d:%02d %d%%'%(t.tm_hour, t.tm_min, b), b < 15 and not c

class TBServer(http.server.BaseHTTPRequestHandler):
    def do_GET(self):
        if self.path == '/shutdown':
            threading.Thread(target=self.server.shutdown).start()
            run_browser.kill_browser()
            self.send_response(200)
            self.send_header('Content-Type', 'text/plain')
            self.send_header('Content-Length', 0)
            self.send_header('Connection', 'close')
            self.end_headers()
            return
        try: idx = int(self.path[1:])
        except ValueError:
            self.send_error(404)
            return
        stl, low_battery = get_statusline()
        until_refresh = 1 if low_battery else (60 - int(time.time())%60)
        ans = ('''\
document.getElementById('tb').innerHTML = %s;
setTimeout(function()
{
    var scr = document.createElement('script');
    scr.src = '/tb/%d';
    document.body.appendChild(scr);
}, %d000);
document.getElementById('lowbattery').style.display = '%s';
'''%(json.dumps(html.escape(stl)), idx+1, until_refresh, 'table' if low_battery else 'none')).encode('utf-8')
        self.send_response(200)
        self.send_header('Content-Type', 'application/javascript')
        self.send_header('Content-Length', len(ans))
        self.send_header('Cache-Control', 'no-cache')
        self.end_headers()
        self.wfile.write(ans)
