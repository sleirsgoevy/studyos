import http.server, html, os.path, json

HTML = '''\
<html>
<body>
<script>
function launchZoom(url)
{
    var width = window.top.screen.width;
    var height = window.top.screen.height;
    var zoom_window = window.open(url, '', 'left=0,top=0,width='+(width/2)+',height='+height);
    var nontexed_window = window.open('/ntwrap.html', '', 'left='+(width/2)+',top=0,width='+(width+1)/2+',height='+height);
    nontexed_window.onload = function()
    {
        nontexed_window.document.getElementById('close_btn').onclick = function()
        {
            zoom_window.close();
            nontexed_window.close();
            window.top.reloadMD();
        };
    };
}
</script>
<div style="display: grid; grid-template-columns: 1fr 1fr 1fr">
%s
</div>
</body>
</html>
'''

def get_timetable():
    with open(os.path.join(os.environ['HOME'], '.studyos', 'timetable.json')) as file:
        return json.load(file)

class TimeTableServer(http.server.BaseHTTPRequestHandler):
    def do_GET(self):
        if self.path != '/':
            self.send_error(404)
        inner = ''
        data = get_timetable()['timetable']
        for i in data:
            inner += '<div style="border: 1px solid black"><h3>'+html.escape(i['day'])+'</h3><ol>'
            for j in i['lessons']:
                if j['url'] is None:
                    inner += '<li>'+html.escape(j['name'])+'</li>'
                else:
                    inner += '<li><a href="'+html.escape('javascript:launchZoom('+json.dumps(j['url'])+')')+'">'+html.escape(j['name'])+'</a></li>'
            inner += '</ol></div>'
        outer = (HTML % inner).encode('utf-8')
        self.send_response(200)
        self.send_header('Content-Type', 'text/html; charset=utf-8')
        self.send_header('Content-Length', len(outer))
        self.end_headers()
        self.wfile.write(outer)

def get_url_whitelist():
    tt = get_timetable()
    ans = tt.get('url_whitelist', [])[:]
    for i in tt['timetable']:
        for j in i['lessons']:
            if j['url'] is not None and (j['url'].startswith('http://') or j['url'].startswith('https://')):
                ans.append(':'.join('/'.join(j['url'].split('/', 3)[:3]).split(':', 2)[:2])+'/*')
    return ans
