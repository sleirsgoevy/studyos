import gateway, run_browser, threading, os.path, subprocess, signal, math, time, datetime, timetable

def run_session():
    subprocess.Popen(('awesome', '-c', os.path.join(os.path.split(__file__)[0] or '.', 'awesome.txt')))
    server_port = gateway.run_server()
    run_browser.run_browser('http://127.0.0.1:%d/'%server_port, ['http://127.0.0.1/*']+timetable.get_url_whitelist(), ['http://127.0.0.1:%d'%server_port])
    os.kill(os.getpid(), signal.SIGKILL)

def parse_time(s):
    cur = time.time()
    tzshift = (datetime.datetime.fromtimestamp(cur) - datetime.datetime.utcfromtimestamp(cur)).seconds
    cur += tzshift
    q = 0
    for i in s.split(':'):
        q = 60 * q + int(i)
    q -= cur
    q %= 86400
    q += cur
    q -= tzshift
    return q

def get_limits():
    try:
        with open(os.path.join(os.environ['HOME'], '.studyos', 'limits.txt')) as file:
            return tuple(map(int, file.read().split()))
    except IOError:
        return 0, 0

def set_limits(a, b):
    with open(os.path.join(os.environ['HOME'], '.studyos', 'limits.txt'), 'w') as file:
        file.write('%d %d\n'%(a, b))

def lsd(until):
    omega1 = 1
    omega2 = 0.5
    omega3 = 4/3
    t0 = time.time()
    while True:
        t = time.time()
        if t >= until: break
        r = 10**(math.atan(t-t0)*math.sin(omega1*t)*2/math.pi)
        g = 10**(math.atan(t-t0)*math.sin(omega2*t)*2/math.pi)
        b = 10**(math.atan(t-t0)*math.sin(omega3*t)*2/math.pi)
        subprocess.call(('xgamma', '-rgamma', str(r), '-ggamma', str(g), '-bgamma', str(b)), stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        time.sleep(0.05)

limits = get_limits()

def daemon():
    t0 = time.time() + 60
    if t0 < limits[0]:
        time.sleep(limits[0] - t0)
        lsd(limits[0])
        os.kill(-1, signal.SIGKILL)
    elif limits[1] - time.time() >= 0:
        lsd(limits[0])
        os.kill(-1, signal.SIGKILL)

def main(*args):
    global limits
    t = time.time()
    if args[0] == 'session':
        if t < limits[0]:
            if not os.fork():
                daemon()
        elif t < limits[1]:
            run_session()
        else:
            os.unsetenv('PYTHONPATH')
            os.execvp(args[1], args[1:])
    elif t < limits[1]:
        print('Cannot setup while in countdown mode')
    else:
        path = os.path.join(os.environ['HOME'], '.studyos', 'secureroot')
        for i in os.listdir(path):
            os.unlink(os.path.join(path, i))
        for i in args[1:]:
            if i.endswith(os.path.sep): i = i[:-1]
            os.symlink(os.path.abspath(i), os.path.join(path, os.path.basename(i)))
        newl = args[0]
        if '/' in newl:
            newstart, newend = newl.split('/')
            newstart = parse_time(newstart)
        else:
            newstart = 0
            newend = newl
        newend = parse_time(newend)
        if newend < newstart:
            print('Cannot setup: end < start')
            return
        limits = (newstart, newend)
        set_limits(*limits)
        if not os.fork():
            daemon()

if __name__ == '__main__':
    import sys
    main(*sys.argv[1:])
