import http.server, os.path, random, json

def songs(path='.'):
    ans = []
    try:
        d = os.listdir(path)
    except OSError:
        if path.endswith('.mp3') or path.endswith('.m4a') or path.endswith('.mp4') or path.endswith('.opus') or path.endswith('.webm'):
            ans.append(path)
    else:
        for i in d: ans.extend(songs(os.path.join(path, i)))
    return ans

class MusicRequestHandler(http.server.BaseHTTPRequestHandler):
    def do_GET(self):
        s = songs()
        if not s: js = b'noMusic();\n'
        else:
            song = '/files/'+random.choice(songs())
            js = ('startTrack('+json.dumps(os.path.basename(song).rsplit('.', 1)[0])+', '+json.dumps(song)+');\n').encode('utf-8')
        self.send_response(200)
        self.send_header('Content-Type', 'application/javascript')
        self.send_header('Content-Length', len(js))
        self.end_headers()
        self.wfile.write(js)
