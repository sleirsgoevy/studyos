import subprocess, tempfile, os.path, json, signal

def get_screen_size():
    p = subprocess.Popen('xrandr', stdout=subprocess.PIPE, encoding='latin-1')
    data = p.communicate()[0]
    p.wait()
    data = data.split()
    idx = data.index('current')
    return int(data[idx+1]), int(data[idx+3].split(',', 1)[0])

the_browser = None

def run_browser(url, whitelist, popup_whitelist=[]):
    width, height = get_screen_size()
    with tempfile.TemporaryDirectory() as path:
        os.makedirs(os.path.join(path, '.mozilla', 'firefox', 'default'))
        os.makedirs(os.path.join(path, '.mozilla', 'firefox', 'default', 'chrome'))
        with open(os.path.join(path, '.mozilla', 'firefox', 'profiles.ini'), 'w') as file:
            file.write("""\
[Profile0]
Name=default
IsRelative=1
Path=default
Default=1

[General]
StartWithLastProfile=1
Version=2

""")
        with open(os.path.join(path, '.mozilla', 'firefox', 'default', 'prefs.js'), 'w') as file:
            file.write("""\
user_pref("browser.tabs.opentabfor.middleclick", false);
user_pref("app.normandy.first_run", false);
user_pref("privacy.webrtc.legacyGlobalIndicator", false);
""")
        with open(os.path.join(path, 'policies.json'), 'w') as file:
            file.write(json.dumps({
                "policies": {
                    "BlockAboutAddons": True,
                    "BlockAboutConfig": True,
                    "BlockAboutProfiles": True,
                    "BlockAboutSupport": True,
                    "DisableDeveloperTools": True,
                    "DisableFeedbackCommands": True,
                    "DisableFirefoxScreenshots": True,
                    "DisableFirefoxAccounts": True,
                    "DisableFirefoxStudies": True,
                    "DisableForgetButton": True,
                    "DisableMasterPasswordCreation": True,
                    "DisablePasswordReveal": True,
                    "DisablePocket": True,
                    "DisablePrivateBrowsing": True,
                    "DisableProfileImport": True,
                    "DisableProfileRefresh": True,
                    "DisableSafeMode": True,
                    "DisableSecurityBypass": {
                        "InvalidCertificates": True
                    },
                    "DisableSetDesktopBackground": True,
                    "DisableSystemAddonUpdate": True,
                    "DisableTelemetry": True,
                    "DisableDeveloperTools": True,
                    "DontCheckDefaultBrowser": True,
                    "DownloadDirectory": "/dev/full",
                    "ManualAppUpdateOnly": True,
                    "PopupBlocking": {
                        "Allow": popup_whitelist,
                    },
                    "PrimaryPassword": False,
                    "NewTabPage": False,
                    "ShowHomeButton": False,
                    "WebsiteFilter": {
                        "Block": ["<all_urls>"],
                        "Exceptions": whitelist
                    }
                }
            }))
        env = dict(os.environ)
        env.update({
            'LD_PRELOAD': os.path.join(os.path.split(__file__)[0] or '.', 'browser', 'libld_preload.so'),
            'POLICY_FILE': os.path.join(path, 'policies.json'),
            'HOME': path,
        })
        global the_browser
        the_browser = subprocess.Popen(('firefox', '--new-instance', '--kiosk', '-width', str(width), '-height', str(height), '-no-remote', url), env=env)
        the_browser.wait()
        the_browser = None

def kill_browser():
    os.kill(the_browser.pid, signal.SIGINT)

if __name__ == '__main__':
    import sys
    run_browser(sys.argv[1], sys.argv[2].split(','), sys.argv[3].split(','))
