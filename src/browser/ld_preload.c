#define _GNU_SOURCE
#include <dlfcn.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <sys/types.h>
#include <xcb/xcb.h>
#include <xcb/randr.h>

/* open policies */

int access(const char* pathname, int mode)
{
    if(!strcmp(pathname, "/etc/firefox/policies/policies.json"))
        return 0;
    static int(*real_access)(const char*, int);
    if(!real_access)
        real_access = dlsym(RTLD_NEXT, "access");
    return real_access(pathname, mode);
}

int open64(const char* pathname, int flags, mode_t mode)
{
    if(!strcmp(pathname, "/etc/firefox/policies/policies.json"))
        pathname = getenv("POLICY_FILE");
    static int(*real_open64)(const char*, int, mode_t);
    if(!real_open64)
        real_open64 = dlsym(RTLD_NEXT, "open64");
    return real_open64(pathname, flags, mode);
}

/* ban keyboard shortcuts */

xcb_generic_event_t* handle_event(xcb_connection_t* conn, xcb_generic_event_t* ev)
{
    if(ev)
    {
        if((ev->response_type & ~0x80) == XCB_KEY_PRESS)
        {
            xcb_key_press_event_t* kp = (xcb_key_press_event_t*)ev;
            if(((kp->state & ~(XCB_MOD_MASK_SHIFT | 0x2000 /* layout */))
            && kp->detail != 22 //Backspace
            && kp->detail != 38 //A
            && kp->detail != 52 //Z
            && kp->detail != 53 //X
            && kp->detail != 54 //C
            && kp->detail != 55 //V
            && kp->detail != 111 //arrows
            && kp->detail != 113
            && kp->detail != 114
            && kp->detail != 116)
            || (kp->detail >= 67 && kp->detail <= 78 && kp->detail != 71))
                return 0;
        }
    }
    return ev;
}

xcb_generic_event_t* xcb_wait_for_event(xcb_connection_t* conn)
{
    xcb_generic_event_t*(*real_xcb_wait_for_event)(xcb_connection_t*);
    if(!real_xcb_wait_for_event)
        real_xcb_wait_for_event = dlsym(RTLD_NEXT, "xcb_wait_for_event");
    return handle_event(conn, real_xcb_wait_for_event(conn));
}

xcb_generic_event_t* xcb_poll_for_event(xcb_connection_t* conn)
{
    xcb_generic_event_t*(*real_xcb_poll_for_event)(xcb_connection_t*);
    if(!real_xcb_poll_for_event)
        real_xcb_poll_for_event = dlsym(RTLD_NEXT, "xcb_poll_for_event");
    return handle_event(conn, real_xcb_poll_for_event(conn));
}

/* disable xshm extension. fixes the camera */
int XShmQueryExtension(void)
{
    return 0;
}
