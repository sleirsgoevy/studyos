import http.server, html, io, sys, urllib.parse, mimetypes, base64

def nop(*args): pass

class FileFragmentWrapper:
    def __init__(self, src, off, l):
        self.src = src
        self.src.seek(off)
        self.l = l
    def read(self, n):
        assert n >= 0
        n = min(n, self.l)
        ans = self.src.read(n)
        self.l -= len(ans)
        return ans

class VerySecureRequestHandler(http.server.SimpleHTTPRequestHandler):
    def list_directory(self, path):
        self.send_response = nop
        self.send_header = nop
        self.end_headers = nop
        ans = http.server.SimpleHTTPRequestHandler.list_directory(self, path).read().decode(sys.getfilesystemencoding())
        del self.send_response
        del self.send_header
        del self.end_headers
        if self.path != '/':
            a, b = ans.split('</h1>\n', 1)
            a, b = (a+'</h1>\n<form method="post" action="#"><input type="submit" value="Upload a file" /></form>\n'+b).split('<ul>\n', 1)
            ans = a+'<ul>\n<li><a href="../">../</a></li>\n'+b
        ans = ans.encode(sys.getfilesystemencoding())
        self.send_response(200)
        self.send_header('Content-Type', 'text/html; charset='+sys.getfilesystemencoding())
        self.send_header('Content-Length', len(ans))
        self.end_headers()
        bb = io.BytesIO()
        bb.write(ans)
        bb.seek(0)
        return bb
    def do_POST(self):
        if self.path == '/':
            self.send_error(403)
            return
        ll = int(self.headers.get('Content-Length', 0))
        data = self.rfile.read(ll)
        if data:
            data = data.decode('ascii')
            params = {k: urllib.parse.unquote(v.replace('+', ' ')) for k, v in (i.split('=', 1) for i in data.split('&'))}
            filename = params['filename']
            data = params['data']
            if '/' in filename or filename in ('.', '..'):
                self.send_error(403)
                return
            with open(self.translate_path(self.path+'/'+filename), 'w') as file: file.write(data)
            self.send_response(301)
            self.send_header('Location', '.')
            self.end_headers()
        else:
            ans = b'''\
<html>
<head>
<title>File upload</title>
</head>
<body>
<form action="#" method="post">
<p><input name="filename" placeholder="File name" /><button type="submit">Upload</button></p>
<textarea style="width: 80%; height: 80%" name="data"></textarea>
</form>
</body>
</html>
'''
            http.server.SimpleHTTPRequestHandler.send_response(self, 200)
            self.send_header('Content-Type', 'text/html; charset='+sys.getfilesystemencoding())
            http.server.SimpleHTTPRequestHandler.send_header(self, 'Content-Length', len(ans))
            self.end_headers()
            self.wfile.write(ans)
    def guess_type(self, path):
        ans = http.server.SimpleHTTPRequestHandler.guess_type(self, path)
        if ans == 'application/octet-stream' or ans.startswith('application/x-'): ans = 'text/plain'
        if ';' not in ans: ans += '; charset=utf-8'
        return ans
    def send_response(self, status, message=None):
        if status == 200 and self.headers.get('Range', '').startswith('bytes='):
            status = 206
            message = None
        return http.server.SimpleHTTPRequestHandler.send_response(self, status, message)
    def send_header(self, header, value):
        if header in ('Cache-Control', 'Last-Modified'): return
        if header == 'Content-Length':
            self.send_header('Accept-Ranges', 'bytes')
            r = self.headers.get('Range', '')
            if r.startswith('bytes='):
                if r.endswith('-'):
                    start = int(r[6:-1])
                    stop = int(value)
                else:
                    start, stop = map(int, r[6:].split('-', 1))
                    stop += 1
                start = min(int(value), max(0, start))
                stop = min(int(value), max(0, stop))
                self.send_header('Content-Range', 'bytes %d-%d/%d'%(start, stop-1, int(value)))
                value = str(stop - start)
                self.copyfile_range = (start, stop)
        return http.server.SimpleHTTPRequestHandler.send_header(self, header, value)
    def end_headers(self):
        self.send_header('Cache-Control', 'no-cache')
        http.server.SimpleHTTPRequestHandler.end_headers(self)
    def copyfile(self, infile, outfile):
        try: start, stop = self.copyfile_range
        except AttributeError: pass
        else: infile = FileFragmentWrapper(infile, start, stop - start)
        return http.server.SimpleHTTPRequestHandler.copyfile(self, infile, outfile)
